---
title: "Experimental"
output: 
  flexdashboard::flex_dashboard:
    orientation: rows
    vertical_layout: fill
---

```{r setup, include=FALSE}
library(flexdashboard)
```

```{r include=FALSE, eval=FALSE}
# Goal: Make multi-page flexdashboard where second tabset changes on click within a page, with:
# (1) Some pages being grouped into menu(s),
# (2) Pages having unequal number of tabs (but N tabs in tabset 1 == N tabs in tabset 2 within a page),
# (3) Each page having 2 tabsets, but ideally, this should work for pages with > 2 tabsets.
# https://stackoverflow.com/questions/61458456/change-second-tabset-on-click-in-flexdashboard/
```

Page 1
==================================

Row {.tabset}
-----------------------------------------------------------------------

### Chart A1
Page 1 Chart A1 is Great

### Chart B1
Page 1 Chart B1 is Great

### Chart C1
Page 1 Chart C1 is Great

### Chart D1
Page 1 Chart D1 is Great

Row {.tabset}
-----------------------------------------------------------------------

### Chart A2
Page 1 Chart A2 is Great

### Chart B2
Page 1 Chart B2 is Great

### Chart C2
Page 1 Chart C2 is Great

### Chart D2
Page 1 Chart D2 is Great


Page 2
==================================

Row {.tabset}
-----------------------------------------------------------------------

### Chart A1
Page 2 Chart A1 is Great

### Chart B1
Page 2 Chart B1 is Great

### Chart C1
Page 2 Chart C1 is Great

Row {.tabset}
-----------------------------------------------------------------------

### Chart A2
Page 2 Chart A2 is Great

### Chart B2
Page 2 Chart B2 is Great

### Chart C2
Page 2 Chart C2 is Great


Page 3 {data-navmenu="Menu 1"}
==================================

Row {.tabset}
-----------------------------------------------------------------------

### Chart A1
Page 3 Chart A1 is Great

### Chart B1
Page 3 Chart B1 is Great

Row {.tabset}
-----------------------------------------------------------------------

### Chart A2
Page 3 Chart A2 is Great

### Chart B2
Page 3 Chart B2 is Great


Page 4 {data-navmenu="Menu 1"}
==================================

Row {.tabset}
-----------------------------------------------------------------------

### Chart A1
Page 4 Chart A1 is Great

### Chart B1
Page 4 Chart B1 is Great

### Chart C1
Page 4 Chart C1 is Great

### Chart D1
Page 4 Chart D1 is Great

### Chart E1
Page 4 Chart E1 is Great

Row {.tabset}
-----------------------------------------------------------------------

### Chart A2
Page 4 Chart A2 is Great

### Chart B2
Page 4 Chart B2 is Great

### Chart C2
Page 4 Chart C2 is Great

### Chart D2
Page 4 Chart D2 is Great

### Chart E2
Page 4 Chart E2 is Great

```{js}
document.addEventListener("DOMContentLoaded", function(){
    $('a[data-toggle="tab"]').on('click', function(e){
      child = e.target.parentNode;
      row = $(e.target).closest("div[id]");
      rowid = row.attr("id");
      tabnum = Array.from(child.parentNode.children).indexOf(child);
      if (rowid == "row") {
        rowto = "row-1";
      } else {
        rowto = "row";
      }
      $("div[id="+rowto+"] li:eq("+tabnum+") a").tab('show');
    })
});
```
